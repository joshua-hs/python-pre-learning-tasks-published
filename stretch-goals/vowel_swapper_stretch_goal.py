def vowel_swapper(string):
    # ==============

    vowelswaps = {
        "a" : ["4", 0],
        "e" : ["3", 0],
        "i" : ["!", 0],
        "o" : ["ooo", 0],
        "u" : ["|_|", 0]
        }

    newstring = ""

    for i in range(0, len(string)):
        char = string[i]
        if char.lower() in vowelswaps:
            vowelswaps[char.lower()][1] += 1
            if vowelswaps[char.lower()][1] == 2:
                if char == "O":
                    newstring += "000"
                    continue
                else:
                    newstring += vowelswaps[char.lower()][0]
                    continue
        newstring += char

    return newstring

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
